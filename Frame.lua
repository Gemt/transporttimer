-- Author   : G3m7
-- Create Date : 6/19/2019 10:13:08 PM
local HBD = LibStub("HereBeDragons-2.0")

TransportTimer_AuberdineTimer = nil
TransportTimer_TheramoreTimer = nil
TransportTimer_RatchetTimer = nil

local TTFrame = CreateFrame("Frame", "TransportTimerFrame", UIParent)
TTFrame:SetBackdrop{
	bgFile = "Interface\\BUTTONS\\WHITE8X8",
	insets = { left = 1, right = 1, top = 1, bottom = 1	 }
}
TTFrame:SetBackdropColor(0, 0, 0, 0.5)
TTFrame:SetClampedToScreen(true)
TTFrame:SetMovable(true)
TTFrame:EnableMouse(true)
TTFrame:SetUserPlaced(true)
TTFrame:RegisterForDrag("LeftButton")
TTFrame:SetScript("OnDragStart", function(self)
    if IsAltKeyDown() then self:StartMoving() end
end)
TTFrame:SetScript("OnDragStop", function(self)
    self:StopMovingOrSizing()
end)

TTFrame:SetPoint("TOPLEFT", UIParent, "TOP", 0, -100)
TTFrame:SetWidth(200)
TTFrame:SetHeight(115)

local function CreateLine(text, row)
    local line = TTFrame:CreateFontString(nil, "ARTWORK")
    line:SetFont("Fonts\\FRIZQT__.TTF", 12)
    
    line:SetPoint("TOPLEFT", TTFrame, "TOPLEFT", 5, -13*(row-1)-5)
    line:SetWidth(155)
    line:SetHeight(10)
    line:SetText(text)
    line:SetJustifyH("LEFT")

    line.value = TTFrame:CreateFontString(nil,"ARTWORK")
    line.value:SetFont("Fonts\\FRIZQT__.TTF", 12)
    line.value:SetPoint("TOPLEFT", line, "TOPRIGHT", 0, 0)
    line.value:SetWidth(60)
    line.value:SetHeight(10)
    line.value:SetJustifyH("LEFT")
    line.value:SetText("nil")
    return line
end

TTFrame.Boats = {
    Auberdine = {
        Name="Auberdine",
        Roundtrip = 295.587,
        Sync1 = {t=31, x=1173.2, y=6112.7, i=1},
        Sync2 = {t=181, x=-377.1, y=-3154.8, i=0},
        Leave1 = 0,
        Arrive2 = 74,
        Leave2 = 140,
        Arrive1 = 230,
        Inverted = 295.587-140, --Roundtrip-Leave2,
        Harbor1 = CreateLine("Menethil => Auberdine", 1),
        Harbor2 = CreateLine("Auberdine => Menethil", 2),
    },
    Theramore = {
        Name="Theramore",
        Roundtrip = 329.3181,
        Sync1 = {t=44, x=-5232.89, y=-3370.1, i=1},
        Sync2 = {t=213, x=326.3999, y=-4020.4001, i=0},
        Leave1 = 0,
        Arrive2 = 100,
        Leave2 = 165,
        Arrive1 = 265,
        Inverted = 329.3181-165, -- Roundtrip-Leave2,
        Harbor1 = CreateLine("Menethil => Theramore", 4),
        Harbor2 = CreateLine("Theramore => Menethil", 5),
    },
    Ratchet = {
        Name="Ratchet",
        Roundtrip = 350.8316923,
        Sync1 = {t=50, x=-4803.1, y=-1327.5 , i=1},
        Sync2 = {t=197, x=863.1, y=-14154.4, i=0},
        Leave1 = 0,
        Arrive2 = 103.5,
        Leave2 = 166.5,
        Arrive1 = 285.7,
        Inverted = 350.8316923-166.5, -- Roundtrip-Leave2,
        Harbor1 = CreateLine("BootyBay => Ratchet", 7),
        Harbor2 = CreateLine("Ratchet => BootyBay", 8),
    }
}

local syncChannelName = "t16ran56sport5792time763r3ync"
C_ChatInfo.RegisterAddonMessagePrefix("TRANSPORT_TIMER")

local nextSync = 0
function DoSync(syncPoint, name)
    timer = GetServerTime() - syncPoint
    nextSync = GetServerTime()+30
    local chans = {GetChannelList()}
    for i=1, getn(chans)/3 do
        local chanId = chans[(i-1)*3+1]
        local chanName = chans[(i-1)*3+2]
        local chanDisabled = chans[(i-1)*3+3]
        --print(tostring(chanId)..", "..chanName..", "..tostring(chanDisabled))
        if chanName == syncChannelName then
            local msg = name..","..tostring(timer)
            local success = C_ChatInfo.SendAddonMessage("TRANSPORT_TIMER", msg, "CHANNEL", chanId)
            return
        end
    end
end

local function UpdateBoat(timer, boat)
    if timer ~= nil then
        local delta = GetServerTime() - timer
        local deltaMod = delta % boat.Roundtrip
        local leaving2 = (boat.Roundtrip-((delta+boat.Inverted) % boat.Roundtrip))
        leaving2 = math.ceil(leaving2)

        local leaving1 = (boat.Roundtrip - (delta % boat.Roundtrip))
        leaving1 = math.ceil(leaving1)

        boat.Harbor1.value:SetText(date("%M:%S", leaving2))
        boat.Harbor2.value:SetText(date("%M:%S", leaving1))
    end

    local playerX, playerY, instId = HBD:GetPlayerWorldPosition()
    if instId == nil or UnitOnTaxi("player") == true then 
        return timer 
    end
    if nextSync > GetServerTime() then
        return timer
    end
    if instId == boat.Sync1.i then
        local dist = HBD:GetWorldDistance(instId, playerX, playerY, boat.Sync1.x, boat.Sync1.y)
        if dist < 20 then
            DoSync(boat.Sync1.t, boat.Name)
        end
    elseif instId == boat.Sync2.i then
        local dist = HBD:GetWorldDistance(instId, playerX, playerY, boat.Sync2.x, boat.Sync2.y)
        if dist < 20 then
            DoSync(boat.Sync2.t, boat.Name)
        end
    end
    return timer
end


TTFrame:SetScript("OnUpdate", function()
    TransportTimer_AuberdineTimer = UpdateBoat(TransportTimer_AuberdineTimer, TTFrame.Boats.Auberdine)
    TransportTimer_TheramoreTimer = UpdateBoat(TransportTimer_TheramoreTimer, TTFrame.Boats.Theramore)
    TransportTimer_RatchetTimer = UpdateBoat(TransportTimer_RatchetTimer, TTFrame.Boats.Ratchet)
end)

TTFrame:RegisterEvent("CHAT_MSG_ADDON")
TTFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
TTFrame:SetScript("OnEvent", function(self, event, ...)
    if event == "CHAT_MSG_ADDON" then 
        local prefix, text, channel, sender, target, zoneChannelID, localID, name, instanceID = ...
        if name == syncChannelName then
            local len = string.len(text)
            local commaIdx = string.find(text,",")
            local boat = string.sub(text, 1, commaIdx-1)
            local val = string.sub(text, commaIdx+1, len)
            print("sync "..boat.." from "..sender)
            if boat == "Auberdine" then
                TransportTimer_AuberdineTimer = tonumber(val)
            elseif boat == "Theramore" then
                TransportTimer_TheramoreTimer = tonumber(val)
            elseif boat == "Ratchet" then
                TransportTimer_RatchetTimer = tonumber(val)
            end
        end
    elseif event == "PLAYER_ENTERING_WORLD" then
        local chans = {GetChannelList()}
        if chans[syncChannelName] == nil then
            JoinChannelByName(syncChannelName)
        end
    end
end)

SLASH_TRANSPORT_TIMER1 = '/tt'
function SlashCmdList.TRANSPORT_TIMER(msg, editbox)
    if TTFrame:IsShown() then
        TTFrame:Hide()
    else
        TTFrame:Show()
    end
end

TTFrame:Hide()


